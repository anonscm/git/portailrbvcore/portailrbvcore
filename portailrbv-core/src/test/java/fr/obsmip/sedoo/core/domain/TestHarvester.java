package fr.obsmip.sedoo.core.domain;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:/META-INF/spring/config-test.xml","classpath:/META-INF/spring/contextProvider.xml","classpath:/META-INF/spring/contacts.xml"})
public class TestHarvester 
{
	final Logger logger =LoggerFactory.getLogger(TestHarvester.class);

	@Test
	public void test() throws Exception 
	{
		Harvester harvester = new Harvester();
		harvester.execute();
	}

		
}
