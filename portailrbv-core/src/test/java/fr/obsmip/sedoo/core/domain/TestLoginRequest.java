package fr.obsmip.sedoo.core.domain;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.obsmip.sedoo.core.dao.GeonetworkUserDAO;
import fr.sedoo.rbv.geonetwork.domain.GeonetworkUser;
import fr.sedoo.rbv.geonetwork.request.LoginRequest;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:/META-INF/spring/config-test.xml","classpath:/META-INF/spring/contextProvider.xml","classpath:/META-INF/spring/contacts.xml"})
public class TestLoginRequest 
{
	final Logger logger =LoggerFactory.getLogger(TestLoginRequest.class);

	@Test
	public void testValidUser() throws Exception 
	{
		LoginRequest loginRequest = new LoginRequest(GeonetworkUserDAO.getUserFromObservatoryShortLabel("ammacatch"));
		loginRequest.execute();
	}
	
	@Test(expected=Exception.class)
	public void testInvalidUser() throws Exception
	{
		LoginRequest loginRequest = new LoginRequest(new GeonetworkUser("fake","fuf"));
		loginRequest.execute();
	}

		
}
