package fr.obsmip.sedoo.core.domain;


import java.util.ArrayList;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.obsmip.sedoo.core.dao.GeonetworkNewDAO;
import fr.sedoo.commons.shared.domain.New;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:/META-INF/spring/config-test.xml","classpath:/META-INF/spring/contextProvider.xml","classpath:/META-INF/spring/contacts.xml"})
public class TestGeonetworkNewDAO 
{
	final Logger logger =LoggerFactory.getLogger(TestGeonetworkNewDAO.class);

	@Test
	public void test() throws Exception 
	{
		GeonetworkNewDAO dao = new GeonetworkNewDAO();
		ArrayList<String> languages = new ArrayList<String>();
		languages.add("fr");
		languages.add("en");
		
		ArrayList<? extends New> latest = dao.getLatest("en", languages);
		Assert.assertFalse("Ne doit pas être vide", latest.isEmpty());
	}
	
	

		
}
