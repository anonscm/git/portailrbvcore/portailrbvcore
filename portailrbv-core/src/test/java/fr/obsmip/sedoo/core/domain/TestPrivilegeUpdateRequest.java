package fr.obsmip.sedoo.core.domain;


import java.util.Collections;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.obsmip.sedoo.core.dao.GeonetworkUserDAO;
import fr.sedoo.rbv.geonetwork.domain.GeonetworkUser;
import fr.sedoo.rbv.geonetwork.request.PrivilegeUdpateRequest;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:/META-INF/spring/config-test.xml","classpath:/META-INF/spring/contextProvider.xml","classpath:/META-INF/spring/contacts.xml"})
public class TestPrivilegeUpdateRequest 
{
	final Logger logger =LoggerFactory.getLogger(TestPrivilegeUpdateRequest.class);

	@Test
	public void test() throws Exception 
	{
		GeonetworkUser ammaUser = GeonetworkUserDAO.getUserFromObservatoryShortLabel("ammacatch");
		PrivilegeUdpateRequest request = new PrivilegeUdpateRequest();
		request.setTargetUser(ammaUser);
		request.setUuids(Collections.singletonList("12"));
		request.execute();
	}

		
}
