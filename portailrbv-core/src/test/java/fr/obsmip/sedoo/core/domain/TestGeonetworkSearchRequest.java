package fr.obsmip.sedoo.core.domain;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.sedoo.rbv.geonetwork.request.GeonetworkSearchRequest;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:/META-INF/spring/config-test.xml","classpath:/META-INF/spring/contextProvider.xml","classpath:/META-INF/spring/contacts.xml"})
public class TestGeonetworkSearchRequest 
{
	final Logger logger =LoggerFactory.getLogger(TestGeonetworkSearchRequest.class);

	@Test
	public void test() throws Exception 
	{
//		GeonetworkUser ammaUser = GeonetworkUserDAO.getUserFromObservatoryShortLabel("ammacatch");
//		PrivilegeUdpateRequest request = new PrivilegeUdpateRequest();
//		request.setTargetUser(ammaUser);
//		request.setUuids(Collections.singletonList("12"));
		GeonetworkSearchRequest request = new GeonetworkSearchRequest();
		request.setObservatoryName("agrhys");
		request.execute();
	}

		
}
