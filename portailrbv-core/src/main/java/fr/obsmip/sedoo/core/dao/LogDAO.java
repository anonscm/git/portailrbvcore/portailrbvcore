package fr.obsmip.sedoo.core.dao;

import java.util.ArrayList;

import fr.obsmip.sedoo.core.domain.Log;

public interface LogDAO {
	static final String LOG_DAO_BEAN_NAME = "logDAO";
	
	ArrayList<Log> findPage(int pageNumber, int pageSize) throws Exception;
	void add(Log log) throws Exception;
	int getHits();

}
