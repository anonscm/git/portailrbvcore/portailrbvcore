package fr.obsmip.sedoo.core.dao;

import java.util.ArrayList;
import java.util.List;

import fr.sedoo.commons.shared.domain.New;

public interface NewsDAO 
{
	
	static final String NEWS_DAO_BEAN_NAME = "newsDAO";
	ArrayList<? extends New> getLatest(String preferredLanguage, List<String> alternateLanguages);
}
