package fr.obsmip.sedoo.core.domain;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import fr.sedoo.commons.metadata.utils.domain.Summary;
import fr.sedoo.commons.shared.domain.New;


public class MetadataUpdateNew implements New {

	private final static SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd");
	
	private static Date DEFAULT_DATE;
	static 
	{
		try {
			DEFAULT_DATE= SDF.parse("1900-01-01");
		} catch (ParseException e) {
			//Nothing to do
		}
	}
	
	private Summary summary;

	public MetadataUpdateNew(Summary summary) {
		this.setSummary(summary);
	}

	public Summary getSummary() {
		return summary;
	}

	public void setSummary(Summary summary) {
		this.summary = summary;
	}

	@Override
	public Date getDate() {
		if (summary.getModificationDate() == null)
		{
			return DEFAULT_DATE;
		}
		else
		{
			try {
				return SDF.parse(summary.getModificationDate());
			} catch (ParseException e) {
				return DEFAULT_DATE;
			}
		}
	}

	@Override
	public String getUuid() {
		return summary.getUuid();
	}

	
	

}
