package fr.obsmip.sedoo.core.dao;

import java.util.List;

public interface ObservatoryNameDAO 
{
	static final String OBSERVATORY_NAME_DAO_BEAN_NAME = "observatoryNameDAO";
	
	/**
	 * Cette fonction retourne la liste des tous les observatoires renseignes dans les fiches de métadonnées
	 * @return
	 */
	public List<String> getObservatoryNameFromEntries();
	
	
	/**
	 * Cette fonction retourne la liste des tous les observatoires renseignes dans le modéle local de données
	 * @return
	 */
	public List<String> getObservatoryNameFromModel();
	
	
	/**
	 * Cette fonction une fusion des deux listes précédentes
	 * @return
	 */
	public List<String> getObservatoryNames();
}
