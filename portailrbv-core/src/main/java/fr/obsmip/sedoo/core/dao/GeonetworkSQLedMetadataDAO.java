package fr.obsmip.sedoo.core.dao;

import org.opengis.metadata.Metadata;

import fr.sedoo.commons.domain.User;
import fr.sedoo.commons.metadata.utils.dao.MetadataDAO;

/**
 * Surchage du DAO Geonetwork qui utilise une requête SQL directe pour les getMetadataById
 * @author andre
 *
 */
public class GeonetworkSQLedMetadataDAO  extends GeonetworkMetadataDAO implements MetadataDAO {

	@Override
	public Metadata getMetadataById(String id, User user) throws Exception {
		
//		MetadataByIdRequest request = null;
//		if (user != null)
//		{
//			request = new MetadataByIdSQLedRequest(id, (GeonetworkUser) user);
//		}
//		else
//		{
//			request = new MetadataByIdSQLedRequest(id);
//		}
//		
//		boolean result = request.execute();
//		if (result == true)
//		{
//			return request.getMetadata();
//		}
//		else
//		{
//			throw new Exception();
//		}
		return null;
	}
	
	
}
