package fr.obsmip.sedoo.core.dao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import fr.sedoo.rbv.geonetwork.request.SuggestRequest;

public class ObservatoryNameDAOImpl implements ObservatoryNameDAO
{
	
	private final String OBSERVATORY_NAME_LUCENE_FIELD_NAME = "observatoryName";

	@Override
	public List<String> getObservatoryNameFromEntries() 
	{
		SuggestRequest request = new SuggestRequest();
		request.setFieldName(OBSERVATORY_NAME_LUCENE_FIELD_NAME);
		try
		{
			boolean result = request.execute();
			if (result == true)
			{
				List<String> suggestions = request.getSuggestions();
				Collections.sort(suggestions);
				return suggestions;
			}
			else
			{
				throw new Exception();
			}
		}
		catch (Exception e)
		{
			return new ArrayList<String>();
		}
	}

	@Override
	public List<String> getObservatoryNameFromModel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> getObservatoryNames() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
