package fr.obsmip.sedoo.core.dao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.sedoo.commons.server.news.dao.MessageDAO;
import fr.sedoo.commons.shared.domain.New;
import fr.sedoo.commons.shared.domain.message.TitledMessage;
public class RBVNewsDAO implements NewsDAO
{

	private static Logger logger = LoggerFactory.getLogger(RBVNewsDAO.class);
	private MessageDAO messageDAO;
	private GeonetworkNewDAO geonetworkNewDAO;
	
	private static final int START_POSITION = 0;
	private static final int PAGE_SIZE = 30;
	
	@Override
	public ArrayList<? extends New> getLatest(String preferredLanguage, List<String> alternateLanguages) 
	{
		logger.debug("RBV News: Début");
		ArrayList<? extends New> latestMetadataModifications = geonetworkNewDAO.getLatest(preferredLanguage, alternateLanguages);
		logger.debug("Nombre de fiches récemment modifiées : "+latestMetadataModifications.size());
		ArrayList<TitledMessage> latestMessages = getLatestMessages(preferredLanguage, alternateLanguages);
		
		ArrayList<New> result = new ArrayList<New>();
		result.addAll(latestMessages);
		result.addAll(latestMetadataModifications);
		
		Collections.sort(result, new NewsComparator());
		
		if (result.size()<=PAGE_SIZE)
		{
			return result;
		}
		else
		{
			return (ArrayList<? extends New>) result.subList(START_POSITION, PAGE_SIZE);
		}
	}
	
	private ArrayList<TitledMessage> getLatestMessages(String preferredLanguage, List<String> alternateLanguages)
	{
		ArrayList<TitledMessage> result = new ArrayList<TitledMessage>();
		
		ArrayList<String> uuids = messageDAO.getUuids();
		for (String uuid : uuids) {
			result.add(messageDAO.getConsultMessageContentByUuid(uuid, preferredLanguage, alternateLanguages)); 
		}
		return result;
	}
	
	public MessageDAO getMessageDAO() {
		return messageDAO;
	}

	public void setMessageDAO(MessageDAO messageDAO) {
		this.messageDAO = messageDAO;
	}

	public GeonetworkNewDAO getGeonetworkNewDAO() {
		return geonetworkNewDAO;
	}

	public void setGeonetworkNewDAO(GeonetworkNewDAO geonetworkNewDAO) {
		this.geonetworkNewDAO = geonetworkNewDAO;
	}

	private class NewsComparator implements Comparator<New>
	{

		@Override
		public int compare(New n1, New n2) 
		{
			if (n1.getDate() == null)
			{
				return -1;
			}
			if (n2.getDate() == null)
			{
				return +1;
			}
			return n2.getDate().compareTo(n1.getDate());
		}
		
	}
	
}
