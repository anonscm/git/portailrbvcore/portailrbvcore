package fr.obsmip.sedoo.core.dao;

import java.util.ArrayList;

import fr.obsmip.sedoo.core.domain.GeonetworkObservatory;

public interface GeonetworkObservatoryDAO {
	
	public final static String BEAN_NAME="geonetworkObservatoryDAO";
	ArrayList<GeonetworkObservatory> findAll();
	void delete(GeonetworkObservatory observatory);
	GeonetworkObservatory save(GeonetworkObservatory observatory);
	GeonetworkObservatory findObservatoryById(Long observatoryId);
	

}
