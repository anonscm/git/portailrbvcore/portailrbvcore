package fr.obsmip.sedoo.core.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.obsmip.sedoo.core.domain.RBVUser;

@Repository
public class UserDAOJPAImpl implements UserDAO{

	protected EntityManager em;
    public EntityManager getEntityManager() {
	        return em;
	    }

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
	        this.em = entityManager;
	    }

	@Transactional
	public void delete(Long id) throws Exception {
		
		RBVUser site = getEntityManager().find(RBVUser.class, id);
		if (site != null)
		{
			getEntityManager().remove(site);
		}
	}

	@Transactional
	public RBVUser save(RBVUser user) 
	{
		if (user.getId() == null)
		{
			getEntityManager().persist(user);
			return user;
		}
		else
		{
			getEntityManager().merge(user);
			return user;
		}
	}

	@Override
	public ArrayList<RBVUser> findAll() {
		List<RBVUser> aux = em.createQuery("select rbvUser from RBVUser rbvUser").getResultList();
		ArrayList<RBVUser> resultList = new ArrayList<RBVUser>(aux);
		return resultList;
	}

	@Override
	public RBVUser login(String login, String password) throws Exception 
	{
		String query = "select rbvUser from RBVUser as rbvUser where rbvUser.login='"+login+"' and rbvUser.password='"+password+"'";
		List<RBVUser> aux = em.createQuery(query).getResultList();
		if (aux.isEmpty())
		{
			throw new Exception();
		}
		else
		{
			return aux.iterator().next();
		}
	}

	@Override
	public RBVUser getExternallyAuthenticatedUser(String login) throws Exception 
	{
		String query = "select rbvUser from RBVUser as rbvUser where rbvUser.login='"+login+"'";
		List<RBVUser> aux = em.createQuery(query).getResultList();
		if (aux.isEmpty())
		{
			throw new Exception("Unknown user");
		}
		else
		{
			return aux.iterator().next();
		}
	}

	
	

	

}
