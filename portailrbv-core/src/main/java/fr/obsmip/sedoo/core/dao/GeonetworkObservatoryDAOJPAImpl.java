package fr.obsmip.sedoo.core.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.obsmip.sedoo.core.domain.GeonetworkObservatory;
import fr.obsmip.sedoo.core.domain.RBVUser;

@Repository
public class GeonetworkObservatoryDAOJPAImpl implements GeonetworkObservatoryDAO{

	protected EntityManager em;
	public EntityManager getEntityManager() {
		return em;
	}

	@PersistenceContext
	public void setEntityManager(EntityManager entityManager) {
		this.em = entityManager;
	}

	@Override
	@Transactional
	public ArrayList<GeonetworkObservatory> findAll() {
		List<GeonetworkObservatory> aux = em.createQuery("select observatory from GeonetworkObservatory observatory").getResultList();
        ArrayList<GeonetworkObservatory> result = new ArrayList<GeonetworkObservatory>();
        result.addAll(aux);
		return result;
	}
	
	@Transactional
	public void delete(GeonetworkObservatory observatory) {
		
		GeonetworkObservatory aux = getEntityManager().find(GeonetworkObservatory.class, observatory.getId());
		if (aux != null)
		{
			getEntityManager().remove(aux);
		}
	}
	
	@Transactional
	public GeonetworkObservatory save(GeonetworkObservatory observatory) 
	{
		if (observatory.getId() == null)
		{
			getEntityManager().persist(observatory);
			return observatory;
		}
		else
		{
			getEntityManager().merge(observatory);
			return observatory;
		}
	}

	@Override
	public GeonetworkObservatory findObservatoryById(Long observatoryId) {
		return getEntityManager().find(GeonetworkObservatory.class, observatoryId);
	}


}