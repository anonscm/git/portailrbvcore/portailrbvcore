package fr.obsmip.sedoo.core.dao;

import java.util.ArrayList;
import java.util.List;

import fr.sedoo.commons.domain.User;
import fr.sedoo.rbv.geonetwork.request.GeoSummary;

public interface GeoSummaryDAO {
	
	String GEOSUMMARY_DAO_BEAN_NAME = "geoSummaryDAO";
	

	List<GeoSummary> getGeoSummaries(User user, String type, ArrayList<String> languages) throws Exception;
}
