package fr.obsmip.sedoo.core.dao;

import java.util.ArrayList;

import fr.obsmip.sedoo.core.domain.ObservatoryPerson;

public interface ObservatoryPersonDAO 
{
	static final String BEAN_NAME = "observatoryPersonDAO";
	ObservatoryPerson save(ObservatoryPerson observatoryPerson) throws Exception;
	void delete(Long id) throws Exception;
	ArrayList<ObservatoryPerson> findByObservatoryId(String observatoryId);
}
