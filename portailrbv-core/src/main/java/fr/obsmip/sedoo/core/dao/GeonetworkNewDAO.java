package fr.obsmip.sedoo.core.dao;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URI;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.ws.rs.core.UriBuilder;

import org.apache.commons.digester3.Digester;
import org.apache.commons.digester3.binder.DigesterLoader;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.InputSource;

import fr.obsmip.sedoo.core.dao.rss.RSSModule;
import fr.obsmip.sedoo.core.dao.rss.UuidWrapper;
import fr.obsmip.sedoo.core.domain.MetadataUpdateNew;
import fr.sedoo.commons.metadata.utils.domain.Summary;
import fr.sedoo.commons.shared.domain.New;
import fr.sedoo.rbv.geonetwork.config.GeonetworkConfig;
import fr.sedoo.rbv.geonetwork.domain.GeonetworkUser;
import fr.sedoo.rbv.geonetwork.request.AuthenticatedRequest;
import fr.sedoo.rbv.geonetwork.request.SummaryByIdRequest;

public class GeonetworkNewDAO extends AuthenticatedRequest implements NewsDAO {

	private static Logger logger = LoggerFactory
			.getLogger(AuthenticatedRequest.class);

	String rssURL = null;

	public GeonetworkNewDAO(GeonetworkUser user) {
		super(user);
	}

	public GeonetworkNewDAO(GeonetworkConfig config) {
		super(config);
	}

	public GeonetworkNewDAO() {
		super();
	}

	@Override
	public ArrayList<? extends New> getLatest(String preferredLanguage,
			List<String> alternateLanguages) {
		logger.debug("Geonetwork DAO : début de la recherche");
		ArrayList<MetadataUpdateNew> news = new ArrayList<MetadataUpdateNew>();
		try {
			if (rssURL == null) {
				GeonetworkConfig config = getConfig();
				rssURL = config.getRssURL();
			}
			URI uri = UriBuilder.fromUri(rssURL).build();

			PostMethod post = new PostMethod(uri.toString());
			String rawXml = executePostMethod(post);

			if (rawXml.length() > 0) {

				DigesterLoader digesterLoader = DigesterLoader
						.newLoader(new RSSModule());
				Digester digester = digesterLoader.newDigester();
				digester.setValidating(false);

				ByteArrayInputStream inputStream = new ByteArrayInputStream(
						rawXml.getBytes());
				Reader reader = new InputStreamReader(inputStream, "UTF-8");
				InputSource is = new InputSource(reader);
				is.setEncoding("UTF-8");

				ArrayList<UuidWrapper> result = digester.parse(is);
				List<String> ids = removeEmptyId(UuidWrapper
						.toStringList(result));
				if (ids.isEmpty() == false) {
					SummaryByIdRequest request = new SummaryByIdRequest();
					request.setIds(ids);
					boolean execute = request.execute();
					if (execute == true) {
						List<Summary> summaries = request.getSummaries();
						Iterator<Summary> iterator = summaries.iterator();
						while (iterator.hasNext()) {
							news.add(new MetadataUpdateNew(iterator.next()));
						}
					}
				}
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return news;
	}

	private List<String> removeEmptyId(List<String> input) {
		List<String> result = new ArrayList<String>();
		Iterator<String> iterator = input.iterator();
		while (iterator.hasNext()) {
			String current = iterator.next();
			if (StringUtils.trimToEmpty(current).length() > 0) {
				if (result.contains(current) == false) {
					result.add(current);
				}
			}
		}

		return result;
	}
}
