package fr.obsmip.sedoo.core.domain;

import java.util.ArrayList;
import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.obsmip.sedoo.core.dao.GeonetworkUserDAO;
import fr.sedoo.rbv.geonetwork.domain.GeonetworkUser;
import fr.sedoo.rbv.geonetwork.request.GeonetworkSearchRequest;
import fr.sedoo.rbv.geonetwork.request.HarvestRequest;
import fr.sedoo.rbv.geonetwork.request.MetadataDeleteRequest;
import fr.sedoo.rbv.geonetwork.request.PrivilegeUdpateRequest;

public class Harvester 
{
	Logger logger = LoggerFactory.getLogger(Harvester.class);
	
	private static final String AMMACATCH = "ammacatch";
	private static final String AGRHYS = "agrhys";

	public void execute() throws Exception
	{
		update(AGRHYS);
		update(AMMACATCH);
	}

	private void update(String observatoryName) throws Exception
	{
		logger.info("Début mise à jour "+observatoryName);
		logger.info("Suppression entrees "+observatoryName);
		deleteHarvestedEntriesByObservatoryName(observatoryName);	
		Thread.sleep(60000);
		logger.info("Moissonage "+observatoryName);
		harvest(observatoryName);
		Thread.sleep(120000);
		logger.info("Mise à jour privilege "+observatoryName);
		updatePrivilege(observatoryName);
		logger.info("Fin mise à jour "+observatoryName);
	}
	
	private void harvest(String observatoryName) throws Exception
	{
		HarvestRequest request = new HarvestRequest();
		request.setObservatoryName(observatoryName);
		request.execute();
		if (!request.isSuccess())
		{
			throw new Exception();
		}
	}


	private void deleteHarvestedEntriesByObservatoryName(String observatoryName) throws Exception
	{
		GeonetworkSearchRequest searchRequest = new GeonetworkSearchRequest();
		searchRequest.setObservatoryName(observatoryName);
		searchRequest.execute();
		delete(searchRequest.getUuids());
	}

	private void delete(ArrayList<String> uuids) throws Exception 
	{
		Iterator<String> iterator = uuids.iterator();
		while (iterator.hasNext()) 
		{
			String uuid = iterator.next();
			MetadataDeleteRequest request = new MetadataDeleteRequest(uuid);
			request.execute();
		}
	}

	
	private void updatePrivilege(String observatoryName) throws Exception 
	{
		GeonetworkUser user = GeonetworkUserDAO.getUserFromObservatoryShortLabel(observatoryName);
		GeonetworkSearchRequest searchRequest = new GeonetworkSearchRequest();
		searchRequest.setObservatoryName(observatoryName);
		searchRequest.execute();
		forceUser(user, searchRequest.getUuids());
	}

	private void forceUser(GeonetworkUser targetUser, ArrayList<String> uuids) throws Exception 
	{
		PrivilegeUdpateRequest request = new PrivilegeUdpateRequest();
		request.setTargetUser(targetUser);
		request.setUuids(uuids);
		request.execute();
		if (!request.isSuccess())
		{
			throw new Exception();
		}
	}
}
