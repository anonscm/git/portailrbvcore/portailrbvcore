package fr.obsmip.sedoo.core.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="RBVUSER")
public class RBVUser {

		@Id
		@Column(name="ID")
		@GeneratedValue(strategy=GenerationType.AUTO)
		private Long id;
		
		@Column(name="LOGIN")
		private String login;
		
		@Column(name="PASSWORD")
		private String password;
		
		@Column(name="NAME")
		private String name;
		
		@Column(name="OBSERVATORIES")
		private String observatories;
		
		@Column(name="ADMIN")
		private boolean admin;
		
		@Column(name="EXTERNAL_AUTHENTICATION")
		private boolean externalAuthentication;

		public String getLogin() {
			return login;
		}

		public void setLogin(String login) {
			this.login = login;
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public String getObservatories() {
			return observatories;
		}

		public void setObservatories(String observatories) {
			this.observatories = observatories;
		}

		public boolean isAdmin() {
			return admin;
		}

		public void setAdmin(boolean admin) {
			this.admin = admin;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public boolean isExternalAuthentication() {
			return externalAuthentication;
		}

		public void setExternalAuthentication(boolean externalAuthentication) {
			this.externalAuthentication = externalAuthentication;
		}
		
	
}
