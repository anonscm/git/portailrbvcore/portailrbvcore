package fr.obsmip.sedoo.core.parameter.parameterprovider;

import javax.servlet.ServletContext;

import fr.sedoo.commons.web.servlet.ServletContextProvider;

public class WebParameterProvider implements ReadableParameterProvider{

	public WebParameterProvider() {
	}
	
	@Override
	public String getParameter(String input) 
	{
		ServletContext context = ServletContextProvider.getContext();
		if (context == null)
		{
			return null;
		}
		else
		{
			return context.getInitParameter(input);
		}
	}

}
