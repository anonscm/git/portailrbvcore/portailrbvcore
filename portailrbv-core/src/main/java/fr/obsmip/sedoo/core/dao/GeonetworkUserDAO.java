package fr.obsmip.sedoo.core.dao;

import java.util.Map;

import fr.obsmip.sedoo.core.RBVApplication;
import fr.sedoo.rbv.geonetwork.domain.GeonetworkUser;
import fr.sedoo.rbv.geonetwork.request.SuggestRequest;

public class GeonetworkUserDAO {
	
	/**
	 * En general le nom des utilisateurs geonetwork est egal à celui du nom de l'observatoire formaté.
	 * Cette liste stocke les exception à cette règle.
	 */
	private static Map<String, String> exceptionMatchingUser;

	public static GeonetworkUser getUserFromObservatoryShortLabel(
			String observatoryName) {
		
		initExceptionMatchingUser();
		String aux = format(observatoryName);
		String login = aux;
		if (exceptionMatchingUser.containsKey(aux))
		{
			login = exceptionMatchingUser.get(aux);
		}
		String password = "sedoo" + login;
		GeonetworkUser geonetworkUser = new GeonetworkUser(login, password);
		geonetworkUser.setFirstName(login);
		geonetworkUser.setLastName(login);
		geonetworkUser.setEmail(login+"@sedoo.fr");
		return geonetworkUser;
	}

	private static String format(String observatoryName) {
		String aux = observatoryName.toLowerCase();
		return aux.replaceAll("[\\p{Punct}]", "");
	}

	public static GeonetworkUser getAdminUser() {
		// On récupère le user Admin via une reqûete
		SuggestRequest request = new SuggestRequest();
		return request.getConfig().getAdminUser();

	}
	
	public static void initExceptionMatchingUser()
	{
		if (exceptionMatchingUser == null)
		{
			exceptionMatchingUser = RBVApplication.getInstance().getExceptionalMatchingUsers();
		}
	}
}
