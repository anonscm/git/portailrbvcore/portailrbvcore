package fr.obsmip.sedoo.core.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = GeonetworkObservatory.TABLE_NAME)
public class GeonetworkObservatory {

	public final static String TABLE_NAME = "GEONETWORK_OBSERVATORY";
	public final static String ID_COLUMN_NAME = "ID";
	public final static String NAME_COLUMN_NAME = "NAME";
	public final static String COLOR_COLUMN_NAME = "COLOR";

	@Id
	@Column(name = GeonetworkObservatory.ID_COLUMN_NAME)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = GeonetworkObservatory.NAME_COLUMN_NAME)
	private String name;

	@Column(name = GeonetworkObservatory.COLOR_COLUMN_NAME)
	private String color;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

}
