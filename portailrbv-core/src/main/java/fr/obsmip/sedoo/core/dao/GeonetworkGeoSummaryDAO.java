package fr.obsmip.sedoo.core.dao;

import java.util.ArrayList;
import java.util.List;

import fr.sedoo.commons.domain.User;
import fr.sedoo.rbv.geonetwork.domain.GeonetworkUser;
import fr.sedoo.rbv.geonetwork.request.GeoSummary;
import fr.sedoo.rbv.geonetwork.request.GeoSummaryRequest;

public class GeonetworkGeoSummaryDAO implements GeoSummaryDAO {

	@Override
	public List<GeoSummary> getGeoSummaries(User user, String type, ArrayList<String> languages) throws Exception
	{  
		 GeoSummaryRequest request = new GeoSummaryRequest((GeonetworkUser) user, type);
		 request.setDisplayLanguages(languages);
		boolean result = request.execute();
		if (result == true)
		{
			return request.getSummaries();
		}
		else
		{
			throw new Exception();
		}
	}

}