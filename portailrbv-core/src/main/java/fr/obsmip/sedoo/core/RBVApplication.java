package fr.obsmip.sedoo.core;

import java.util.Map;

import fr.obsmip.sedoo.core.dao.GeoSummaryDAO;
import fr.obsmip.sedoo.core.dao.GeonetworkObservatoryDAO;
import fr.obsmip.sedoo.core.dao.LogDAO;
import fr.obsmip.sedoo.core.dao.NewsDAO;
import fr.obsmip.sedoo.core.dao.ObservatoryPersonDAO;
import fr.obsmip.sedoo.core.dao.UserDAO;
import fr.obsmip.sedoo.core.domain.RBVUser;
import fr.sedoo.commons.ioc.BeanFactory;
import fr.sedoo.commons.metadata.utils.dao.MetadataDAO;
import fr.sedoo.commons.server.news.dao.MessageDAO;
import fr.sedoo.commons.spring.SpringBeanFactory;

public class RBVApplication  
{
	private BeanFactory beanFactory;

	protected static RBVApplication instance;

	protected RBVApplication()
	{
		super();
	}


	public static RBVApplication getInstance() {
		if (instance == null)
		{
			instance = new RBVApplication();

		}
		return instance;	}



	public void start() {

	}



	public void stop() {

	}




	public BeanFactory getBeanFactory() 
	{
		if (beanFactory == null)
		{
			beanFactory = new SpringBeanFactory();
		}
		return beanFactory;
	}


	public MetadataDAO getMetadataDAO() 
	{
		return (MetadataDAO) getBeanFactory().getBeanByName(MetadataDAO.METADATA_DAO_BEAN_NAME);
	}
	
	public GeonetworkObservatoryDAO getGeonetworkObservatoryDAO() 
	{
		return (GeonetworkObservatoryDAO) getBeanFactory().getBeanByName(GeonetworkObservatoryDAO.BEAN_NAME);
	}


	public NewsDAO getNewsDAO() {
		return (NewsDAO) getBeanFactory().getBeanByName(NewsDAO.NEWS_DAO_BEAN_NAME);
	}
	

	public UserDAO getUserDAO() {
		return (UserDAO) getBeanFactory().getBeanByName(UserDAO.USER_DAO_BEAN_NAME);
	}

	public GeoSummaryDAO getGeoSummaryDAO() {
		return (GeoSummaryDAO) getBeanFactory().getBeanByName(GeoSummaryDAO.GEOSUMMARY_DAO_BEAN_NAME);
	}


	public MessageDAO getMessageDAO() {
		return (MessageDAO) getBeanFactory().getBeanByName(MessageDAO.BEAN_NAME);
	}
	
	public LogDAO getLogDAO() {
		return (LogDAO) getBeanFactory().getBeanByName(LogDAO.LOG_DAO_BEAN_NAME);
	}


	public Map<String, RBVUser> getSuperAdmins() 
	{
		return (Map<String, RBVUser>) getBeanFactory().getBeanByName(UserDAO.SUPER_ADMIN_MAP_NAME);
	}


	public Map<String, String> getExceptionalMatchingUsers()
	{
		return (Map<String, String>) getBeanFactory().getBeanByName(UserDAO.EXCEPTION_MATCHING_USER_MAP_NAME);
	}


	public ObservatoryPersonDAO getObservatoryPersonDAO() {
		return (ObservatoryPersonDAO) getBeanFactory().getBeanByName(ObservatoryPersonDAO.BEAN_NAME);
	}
}
