package fr.obsmip.sedoo.core.dao;

import java.util.ArrayList;

import fr.obsmip.sedoo.core.domain.RBVUser;

public interface UserDAO {
	static final String USER_DAO_BEAN_NAME = "userDAO";
	static final String SUPER_ADMIN_MAP_NAME = "superAdmins";
	static final String EXCEPTION_MATCHING_USER_MAP_NAME = "exceptionMatchingUsers";
	
	ArrayList<RBVUser> findAll() throws Exception;

	void delete(Long id) throws Exception;

	RBVUser save(RBVUser user);

	RBVUser login(String login, String password) throws Exception;

	RBVUser getExternallyAuthenticatedUser(String login) throws Exception;

}
